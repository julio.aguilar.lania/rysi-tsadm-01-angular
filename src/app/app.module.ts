import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Alumnos } from './alumnos/alumnos.component';
import { FormsModule } from '@angular/forms';
import { AlumnoComponent } from './alumnos/alumno.component';
import { AlumnosService } from './alumnos/alumnos.service';

@NgModule({
  declarations: [
    AppComponent, Alumnos, AlumnoComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [AlumnosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
