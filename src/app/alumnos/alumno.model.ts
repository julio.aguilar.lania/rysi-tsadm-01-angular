export class Alumno {
  nombre: string;
  apellido: string;
  matricula: string;
  activo: boolean;
}
