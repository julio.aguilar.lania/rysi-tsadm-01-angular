import { Component, OnInit } from "@angular/core";
import { Alumno } from "./alumno.model";
import { AlumnosService } from "./alumnos.service";

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css']
})
export class Alumnos implements OnInit {
  alumnos: Alumno[];
  nuevoAlumno: Alumno;
  modoEdicion: boolean = false;

  constructor(private alService: AlumnosService) {}

  ngOnInit(): void {
    this.alumnos = this.alService.getAlumnos();
    this.nuevoAlumno = {nombre:"",apellido:"",matricula:"",activo:false};
  }

  onGuardarAlumno() {
    console.log("onGuardarAlumno");
    this.nuevoAlumno.activo = true;

    if (this.modoEdicion) {
      this.alService.guardarAlumno(this.nuevoAlumno);
    }
    else {
      this.alService.agregarAlumno(this.nuevoAlumno);
    }
    this.nuevoAlumno = {nombre:"",apellido:"",matricula:"",activo:false};
    this.modoEdicion = false;

    // Ciclo para buscar un alumno con la misma matricula
    // this.alumnos.find(al => al.matricula === this.nuevoAlumno.matricula)

    // Splice permite quitar/reemplazar elementos dentro de un arreglo
    // this.alumnos.splice.
  }

  onEditarAlumno(al:Alumno) {
    console.log("onEditarAlumno");
    console.log(al);
    this.nuevoAlumno = al;
    this.modoEdicion = true;
  }

  onClickAlumno(al: Alumno) {
    console.log("Click en alumno", al.matricula);
  }
}
