import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Alumno } from "./alumno.model";

@Component({
  selector:'app-alumno',
  templateUrl:'./alumno.component.html',
  styleUrls:['./alumno.component.css']
})
export class AlumnoComponent {
  @Input()
  alumno: Alumno;

  @Output()
  seleccionarAlumno = new EventEmitter();

  onSelectAlumno() {
    console.log("onSelectAlumno");
    this.seleccionarAlumno.emit(this.alumno);
  }

}
