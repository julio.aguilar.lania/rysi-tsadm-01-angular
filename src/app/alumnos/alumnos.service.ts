import { Alumno } from "./alumno.model";

export class AlumnosService {
  private listaAlumnos: Alumno[];

  constructor() {
    this.listaAlumnos = [{nombre:"Julio", apellido:"Aguilar", matricula:"1234567ABC", activo: true}, {
      nombre:"Pedro", apellido:"Páramo", matricula:"1234568ABD", activo: false},
    {nombre: "Fulano", apellido:"De Tal", matricula:"9876543WER", activo: true}];
  }

  getAlumnos() {
    console.log("SRV:getAlumnos()");
    return this.listaAlumnos;
  }

  getAlumnoPorMatricula(matricula: string) {
    return this.listaAlumnos.find(al => al.matricula === matricula);
  }

  agregarAlumno(al:Alumno) {
    console.log("SRV:agregarAlumno()");
    this.listaAlumnos.push(al);
  }

  guardarAlumno(al:Alumno) {
    console.log("SRV:guardarAlumno()");
    this.listaAlumnos = this.listaAlumnos.filter(alum => alum.matricula !== al.matricula);
    this.listaAlumnos.push(al);
  }


}
